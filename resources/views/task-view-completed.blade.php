@extends('layouts.app')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>{{$task->description}}</h1>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p>Создал: {{$task->created_at}}
					<br>
					<br>
					<b>{{$creator->full_name}}</b>({{$creator->department->name}})
					<br>
					<br>
				</p>
			</div>
			<div class="col-md-6">
				<p>Исполнитель:&nbsp;
					<br>
					<br>
					<b>{{$task->user->full_name}}</b>&nbsp;({{$task->user->department->name}})
					<br>
					<br>
				</p>
			</div>
		</div>
	</div>
</div>
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Выполнено {{$task->updated_at}}</h2>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>{{$task->additionalInfo}}</p>
			</div>
		</div>
	</div>
</div>
<div class="section">
	<div class="container">
		<div class="row">
			@foreach($imagesFinish as $img)
			<div class="col-md-3">
				<img src="{{$img->name}}" class="img-responsive img-rounded">
			</div>
			@endforeach
		</div>
	</div>
</div>
<div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<hr>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Задание:</h2>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>{{$task->information}}</p>
			</div>
		</div>
	</div>
</div>
<div class="section">
	<div class="container">
		<div class="row">
			@foreach($imagesStart as $img)
			<div class="col-md-3">
				<img src="{{$img->name}}" class="img-responsive img-rounded">
			</div>
			@endforeach
		</div>
	</div>
</div>
@if(Session::has('message'))
<div class="alert alert-success" role="alert">
	{{Session::get('message')}}
</div>
@endif
@if (count($errors) > 0)
<ul>
	@foreach ($errors->all() as $error)
	<div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
	@endforeach
</ul>
@endif
<script type="text/javascript" async="" src="main.js"></script>
@endsection