@extends('layouts.app')
@section('content')
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<form method="POST" action="{{url('task-create')}}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
    <!-- <meta name="csrf-token" content="{!! csrf_token() !!}">  -->
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <h1>Новая задача</h1>
                </div>
            </div>
        </div>
    </div>

<div class="section">
    <div class="container">
        <div class="row">
                <!-- Область предпросмотра -->
                <div id="uploaded-holder"> 
                    <div id="dropped-files">
                        <!-- Кнопки загрузить и удалить, а также количество файлов -->
                        <div id="upload-button">
                            <center>
                                <span>0 Файлов</span>
                                <a href="#" class="upload">Загрузить</a>
                                <a href="#" class="delete">Удалить</a>
                                <!-- Прогресс бар загрузки -->
                                <div id="loading">
                                    <div id="loading-bar">
                                        <div class="loading-color"></div>
                                    </div>
                                    <div id="loading-content"></div>
                                </div>
                            </center>
                        </div>  
                    </div>
                </div>
<!--             <div class="col-md-3">
                <img class="img-responsive img-rounded" src="http://st.depositphotos.com/2313517/3165/i/950/depositphotos_31654589-stock-photo-dropped-ice-cream-cone.jpg"><a class="btn btn-block btn-default">Удалить</a>
            </div>
            <div class="col-md-3">
                <img src="http://st.depositphotos.com/2313517/3165/i/950/depositphotos_31654589-stock-photo-dropped-ice-cream-cone.jpg" class="img-responsive img-rounded"><a class="btn btn-block btn-default">Удалить</a>
            </div> -->
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form role="form">
                    <div class="form-group">
                        <label class="control-label">Фото</label>


                        <form id="frm">
                            <input type="file" id="uploadbtn" name="image[]" multiple />
                        </form>
                        <!-- Список загруженных файлов -->
                        <div id="file-name-holder">
                            <ul id="uploaded-files">
                                <h1>Загруженные файлы</h1>
                            </ul>
                        </div>

                    <!-- <input type="file" name="image[]"> -->
                    </div>
                    <label class="control-label">Добавить фото</label>
                    <br>
                    <button class="btn btn-primary add_images" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                    <div class="form-group">
                        <label class="control-label">Описание:</label>
                        <input class="form-control" type="text" name="description">
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label class="radio-inline">
                                <input type="radio" name="urgently" id="optionsRadios1" value="1" checked="">Срочно</label>
                                <label class="radio-inline">
                                    <input type="radio" name="urgently" id="optionsRadios2" value="0" checked="">&nbsp;Сегодня</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Отдел</label>
                                <select class="form-control" name="department_id" id="department">
                                    <option disabled selected>Выберите отдел</option>
                                    @foreach($departments as $department)
                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                    @endforeach
                                </select>                                    
                            </div>
                            <div class="form-group">
                                <label class="control-label">Сотрудник</label>
                                <select class="form-control" name="user_id" id="user">
                                    <option disabled selected>Выберите сотрудника</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Дополнительная информация</label>
                                <textarea class="form-control" name="information"></textarea></div><button type="submit" class="btn btn-block btn-default btn-lg">Добавить</button></form>
                            </div>
                        </div>
                    </div>
                </div>                
            </form>   
            <script>
                jQuery(document).ready(function($){
                    $('#department').change(function(){
                        $.ajaxSetup({
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                        $.get("{{ url('task-create/dropdown')}}", 
                            { option: $(this).val() }, 
                            function(data) {
                                var user = $('#user');
                                user.empty();

                                $.each(data, function(index, element) {
                                    user.append("<option value='"+ element.id +"'>" + element.full_name + "</option>");
                                });
                            });
                    });
                });
            </script>
            @if(Session::has('message'))
            <div class="alert alert-success" role="alert">
                {{Session::get('message')}}
            </div>
            @endif
            @if (count($errors) > 0)
            <ul>
                @foreach ($errors->all() as $error)
                <div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
                @endforeach
            </ul>
            @endif
            @endsection

