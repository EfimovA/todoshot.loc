@extends('layouts.app')
@section('content')
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<form method="POST" action="{{action('TasksController@doneUpdate', [$task->id])}}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
    <br>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{action('TasksController@edit',[$task->id])}}" class="btn btn-primary">Изменить</a>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{$task->description}}</h1>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Создал: {{$task->updated_at}}
                        <br>
                        <br>
                        <b>{{$creator->full_name}}&nbsp;</b>({{$creator->department->name}})
                        <br>
                        <br>
                        <a href="tel:{{$creator->phone}}">{{$creator->phone}}</a>
                        <br>
                        <br>
                        <br>
                    </p>
                </div>
                <div class="col-md-6">
                    <p>Исполнитель:&nbsp;
                        <br>
                        <br>
                        <b>{{$task->user->full_name}}</b>&nbsp;({{$task->user->department->name}})
                        <br>
                        <br>
                        <a href="tel:{{$task->user->phone}}">{{$task->user->phone}}</a>
                        <br>
                        <br>
                        <br>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>{{$task->information}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                @foreach($images as $image)
                <div class="col-md-3">
                    <img src="{{$image->name}}" class="img-responsive img-rounded">
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form role="form">
                        <div class="form-group">
                            <label class="control-label">Фото</label>
                            <input type="file" name="image[]">
                            <p class="help-block">Без фотографии выполнение задачи невозможно</p>
                            <label class="control-label">Добавить фото</label>
                            <br>
                            <button class="btn btn-primary add_images" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                        </div>
                        <div class="form-group">
                            <label class="control-label"></label>
                            <input class="form-control" type="text" name="additionalInfo" placeholder="Дополнительная информация">
                        </div>
                        <button type="submit" class="btn btn-block btn-lg btn-success">Сделано</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-default">Сделаю завтра</a>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success" role="alert">
        {{Session::get('message')}}
    </div>
    @endif
    @if (count($errors) > 0)
    <ul>
        @foreach ($errors->all() as $error)
        <div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
        @endforeach
    </ul>
    @endif
    @endsection