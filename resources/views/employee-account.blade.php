@extends('layouts.app')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">			
				<form method="POST" action="{{url('my-profile')}}">
					<div class="form-group">
						<label class="control-label">ФИО</label>
						<input class="form-control" type="text" name="full_name"value="{{$user->full_name}}">
					</div>
					<div class="form-group">
						<label class="control-label">Телефон</label>
						<input class="form-control" type="text" name="phone" value="{{$user->phone}}"></div>
						<div class="form-group"><label class="control-label">Эл. почта</label>
							<input class="form-control" type="text" name="" value="{{$user->email}}" disabled="disabled"></div>
							<div class="form-group">
								<label class="control-label">Отдел</label>		
								<select class="form-control" name="department_id" id="department">
									<option name="department_id" value="{{$departmentOfUser->id}}" disabled selected>{{$departmentOfUser->name}}</option>
									@foreach($departments as $department)
									<option name="department_id" value="{{$department->id}}">{{$department->name}}</option>
									@endforeach
								</select>
								<br>									
								<button type="submit" class="btn btn-block btn-default">Сохранить</button>
								<input type="hidden" name="_token" value="{{csrf_token()}}">
							</form>								
						</div>
					</div>
				</div>
				@if(Session::has('message'))
				<div class="alert alert-success" role="alert">
					{{Session::get('message')}}
				</div>
				@endif
				@if (count($errors) > 0)
				<ul>
					@foreach ($errors->all() as $error)
					<div class="alert alert-warning" role="alert"><li>{{ $error }}</li></div>
					@endforeach
				</ul>
				@endif
			</div>	
			@endsection