@extends('layouts.app')

@section('navigation')
@endsection

@section('content')
<br>
<div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<a href="/task-create" class="btn btn-lg btn-success">Новая задача</a>
			</div>
		</div>
	</div>
</div>

<div class="section" style="margin-top: 20px">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12 text-center">
					<div class="btn-group">
						<a href="{{url('allTasks')}}" class="active btn btn-default">Все</a>
						<a href="{{url('urgentlyTask')}}" class="btn btn-default">Срочно</a>
						<a href="{{url('todayTask')}}" class="btn btn-default">Сегодня</a>
						<a href="{{url('doneTask')}}" class="btn btn-default">Готовые</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" async="" src="main.js"></script>


<div class="section" style="margin-top: 20px">
	<div class="container">
		<div class="row">
			@foreach($tasks as $task)
			<div class="col-md-3 task-item">
				<form method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
					@if ($task->done == 0)		
					<a href="{{action('TasksController@viewTask', $task->id)}}" type="submit">
						@elseif ($task->done == 1)
						<a href="{{action('TasksController@viewTaskCompleted', $task->id)}}" type="submit">
							@endif
							<img src="{{$images[$loop->index]}}" class="img-responsive img-rounded">
							<h3>{{$task->description}}</h3>
							<input type="hidden" name="task_id" value="{{ $task->id }}"/>
						</a>
					</form>
				</div>
				@endforeach
				<div class="col-md-12 text-center">
					<nav aria-label="Page navigation">
					<ul class="pagination pagination-lg">
							<li>
								<a href="{{$tasks->previousPageUrl()}}" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li><a href="{{$tasks->url(1)}}">1</a></li>
							@for($i=2; $i<$tasks->count()-1; $i++)
							<li><a href="{{$tasks->nextPageUrl()}}">{{$i}}</a></li>
							@endfor
							<li>
								<a href="{{$tasks->nextPageUrl()}}" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	@endsection