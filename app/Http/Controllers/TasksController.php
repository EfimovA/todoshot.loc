<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\User;
use App\Task;
use App\Image;
use Auth;
use Intervention\Image\ImageManager;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::where('user_id', '=', Auth::user()->id)->where('done','=','0')->paginate(4);
        $images=[];
        foreach ($tasks as $task) {
            $image = Image::where('task_id', '=', $task->id)->first();
            $images[]=$image->name;
        }
        return view('welcome', compact('tasks', 'images'));
    }

    public function indexUrgently()
    {
        $tasks = Task::where('user_id', '=', Auth::user()->id)->where('done','=','0')->where('urgently','=','1')->paginate(4);
        $images=[];      
        foreach ($tasks as $task) {
            $image = Image::where('task_id', '=', $task->id)->first();
            $images[]=$image->name;
        }
        return view('welcome', compact('tasks', 'images'));
    }

    public function indexToday()
    {
        $tasks = Task::where('user_id', '=', Auth::user()->id)->where('done','=','0')->where('urgently','=','0')->paginate(4);
        $images=[];
        foreach ($tasks as $task) {
            $image = Image::where('task_id', '=', $task->id)->first();
            $images[]=$image->name;
        }
        return view('welcome', compact('tasks', 'images'));
    }

    public function indexDone()
    {
        $tasks = Task::where('user_id', '=', Auth::user()->id)->where('done','=','1')->paginate(4);
        $images=[];
        foreach ($tasks as $task) {
            $image = Image::where('task_id', '=', $task->id)->first();
            $images[]=$image->name;
        }
        return view('welcome', compact('tasks', 'images'));
    }

    public function viewTask($id)
    {
        $task = Task::find($id);
        $images = Image::where('task_id', '=', $task->id)->get();
        $creator = User::where('id','=', $task->creator)->get()->first();
        return view('task-view', compact('task', 'images', 'creator'));
    }

    public function viewTaskCompleted($id)
    {
        $task = Task::find($id);
        $imagesStart = Image::where('task_id', '=', $task->id)->where('isDone',false)->get();
        $imagesFinish = Image::where('task_id', '=', $task->id)->where('isDone',true)->get();
        $creator = User::where('id','=', $task->creator)->get()->first();
        return view('task-view-completed', compact('task','imagesStart','imagesFinish','creator'));
    }

    public function doneUpdate($id, Request $request)
    {
        $this->validate($request, [
            'additionalInfo' => 'required|max:255',
            'image' => 'required',
        ]);
        $task = Task::find($id);        
        $task->additionalInfo = $request->additionalInfo;      
        $task->done = true;  
        $task->save();

        $path = public_path()."/images/"; //определяем папку для сохранения картинок


                var_dump($request->file('image'));
             // массив, который будет содержать ссылки на все картинки
                if (is_array($request->file('image'))){
                    foreach($request->file('image') as $file) //обрабатываем массив с файлами
                    {
                        if(empty($file)) continue;
                        // если <input type="file"... есть, но туда ничего не загруженно, то пропускаем
                        $extension = $file->getClientOriginalExtension();
                        $filename = TasksController::getRandomFileName($path, $extension);
//dump($f_name);
                        $url_img='/images/'.$filename.'.'.$extension; //добавляем url картинки в массив

                        
                        $manager = new ImageManager(array('driver' => 'imagick'));
                        $image = $manager->make($file)->resize(300, 200)->save($path . $filename.'.'.$extension);
//dump($root . $f_name);
                        $image = new Image;
                        $image->task_id = $task->id;
                        $image->task()->associate($task->id);
                        $image->name = $url_img;
                        $image->isDone = true;                        
                        $image->save();

                    }//die();
                }
                $imagesStart = Image::where('task_id', '=', $task->id)->where('isDone',false)->get();
                $imagesFinish = Image::where('task_id', '=', $task->id)->where('isDone',true)->get();
                $creator = User::where('id','=', $task->creator)->get()->first();
                return view('task-view-completed', compact('task','imagesStart','imagesFinish','creator'))->with('message', 'Задание выполнено');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('task-create', compact('departments'));
    }

    public static function getRandomFileName($path, $extension='')
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';
 
        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($file));
 
        return $name;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|max:255',
            'information' => 'required|max:255',
            'image' => 'required'
        ]);
        $task = new Task;
        $task->description = $request->description;
        $task->information = $request->information;
        $task->urgently = $request->urgently;
        $task->done = false;
        $task->creator = Auth::user()->id;
        $task->user()->associate($request->user_id);
        $task->save();

        $path = public_path()."/images/"; //определяем папку для сохранения картинок


                var_dump($request->file('image'));
             // массив, который будет содержать ссылки на все картинки
                if (is_array($request->file('image'))){
                    foreach($request->file('image') as $file) //обрабатываем массив с файлами
                    {
                        if(empty($file)) continue;
                        // если <input type="file"... есть, но туда ничего не загруженно, то пропускаем
                        $extension = $file->getClientOriginalExtension();
                        $filename = TasksController::getRandomFileName($path, $extension);
//dump($f_name);
                        $url_img='/images/'.$filename.'.'.$extension; //добавляем url картинки в массив

                        
                        $manager = new ImageManager(array('driver' => 'imagick'));
                        $image = $manager->make($file)->resize(300, 200)->save($path . $filename.'.'.$extension);
//dump($root . $f_name);
                        $image = new Image;
                        $image->task_id = $task->id;
                        $image->task()->associate($task->id);
                        $image->isDone = false;
                        $image->name = $url_img;                        
                        $image->save();

                    }//die();
                }
                return back()->with('message', 'Задание добавлено');                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        $departments = Department::all();
        return view('task-edit', compact('task', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required|max:255',
            'information' => 'required|max:255',
        ]);
        $task = Task::find($id);
        $task->description = $request->description;
        $task->information = $request->information;
        $task->urgently = $request->urgently;
        $task->done = false;
        $task->creator = Auth::user()->id;
        if ($request->user_id != null){
        $task->user()->associate($request->user_id);
        }
        $task->save();

        $path = public_path()."/images/"; //определяем папку для сохранения картинок


                var_dump($request->file('image'));
             // массив, который будет содержать ссылки на все картинки
                if (is_array($request->file('image'))){
                    foreach($request->file('image') as $file) //обрабатываем массив с файлами
                    {
                        if(empty($file)) continue;
                        // если <input type="file"... есть, но туда ничего не загруженно, то пропускаем
                        $extension = $file->getClientOriginalExtension();
                        $filename = TasksController::getRandomFileName($path, $extension);
//dump($f_name);
                        $url_img='/images/'.$filename.'.'.$extension; //добавляем url картинки в массив

                        
                        $manager = new ImageManager(array('driver' => 'imagick'));
                        $image = $manager->make($file)->resize(300, 200)->save($path . $filename.'.'.$extension);
//dump($root . $f_name);
                        $image = new Image;
                        $image->task_id = $task->id;
                        $image->task()->associate($task->id);
                        $image->isDone = false;
                        $image->name = $url_img;                        
                        $image->save();

                    }//die();
                }
                return back()->with('message', 'Задание добавлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
