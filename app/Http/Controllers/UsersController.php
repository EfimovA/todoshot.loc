<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Department;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $department_id = Auth::user()->department_id;
        $user = Auth::user();
        $departments = Department::all(); 
        $departmentOfUser = User::find($user)->department()->get()->first();
        return view('employee-account', compact('user', 'departments', 'departmentOfUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required|max:255|regex:/[а-яА-ЯёЁ]+\s[а-яА-ЯёЁ]+\s[а-яА-ЯёЁ]+/u',
            'phone' => 'regex:/(\+){1}[\d]{1}(\s){1}(\d){3}(\s){1}(\d){3}(\s){1}(\d){2}(\s){1}(\d){2}/',
        ]);
        $user = Auth::user();
        $user->full_name = $request->full_name;
        $user->phone = $request->phone;
        if ($request->department_id != null){
        $user->department_id = $request->department_id;        
        }
        $user->save();
        return back()->with('message', 'Данные пользователя обновлены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
