<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

	Route::get('/', function () {
		return redirect(url('allTasks'));
	});
		Route::get('my-profile', 'UsersController@edit');
		Route::post('my-profile', 'UsersController@update');
		Route::get('task-create', 'TasksController@create');
		Route::post('task-create', 'TasksController@store');
		Route::get('task-edit/{id}', 'TasksController@edit');
		Route::post('task-edit/{id}', 'TasksController@update');
		Route::get('allTasks', 'TasksController@index');
		Route::get('urgentlyTask', 'TasksController@indexUrgently');
		Route::get('todayTask', 'TasksController@indexToday');
		Route::get('doneTask', 'TasksController@indexDone');
		Route::get('task-view-completed/{id}', 'TasksController@viewTaskCompleted');
		Route::match(['get','post'],'task-view/{id}', 'TasksController@viewTask');
		// Route::match(['get','post'],'task-view-completed/{id}', 'TasksController@doneUpdate');
		Route::post('task-view-completed/{id}', 'TasksController@doneUpdate');
		Route::post('del_image','ImagesController@del_image');

		Route::get('task-create/dropdown', function(){
		  	$input = Illuminate\Support\Facades\Input::get('option');
			$department = App\Department::find($input);
			$users = $department->user();
			return Response::json($users->get(['id','full_name']));
		});

		// Route::group(['middleware' => 'admin'], function () {
		// 	Route::get('admin/my-profile', 'AdminController@edit');
		// });


});
